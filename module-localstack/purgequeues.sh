aws sqs purge-queue --endpoint-url=http://localhost:4576 --queue-url http://localhost:4576/queue/sa-in-dev
aws sqs purge-queue --endpoint-url=http://localhost:4576 --queue-url http://localhost:4576/queue/sa-out-dev

aws --endpoint-url=http://localhost:4572 s3 rm s3://sa-in-dev --recursive
aws --endpoint-url=http://localhost:4572 s3 rm s3://sa-out-dev --recursive