aws --endpoint-url=http://localhost:4576 sqs create-queue --queue-name sa-in-dev
aws --endpoint-url=http://localhost:4576 sqs create-queue --queue-name sa-out-dev
aws --endpoint-url=http://localhost:4576 sqs get-queue-attributes --queue-url http://localhost:4576/queue/sa-in-dev --attribute-names All
aws --endpoint-url=http://localhost:4576 sqs get-queue-attributes --queue-url http://localhost:4576/queue/sa-out-dev --attribute-names All